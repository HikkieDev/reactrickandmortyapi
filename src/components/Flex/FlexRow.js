import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import styles from "./styles";

const useStyles = makeStyles(styles);

export default function FlexRow(props) {
    const { children } = props;
    const classes = useStyles();
    return (
        <div className={classes.flex}>
            {children}
        </div>
    )
}