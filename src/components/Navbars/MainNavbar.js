import React,{ Component } from "react";
import Search from "./Search";

import { FilterContext } from "../../components/FilterContext";

export default class Navbar extends Component {
    static contextType = FilterContext

    constructor(props) {
        super(props);

        this.state = { Find: "", ShowMenu: false}
    }

    render() {
        const { setFilter } = this.context

        return (
            <>
                <Search Update={setFilter}/>
            </>
        );
    }
}