/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import ExploreIcon from '@material-ui/icons/Explore';
import TheatersIcon from '@material-ui/icons/Theaters';
// core components/views for Admin layout
import Home from "views/Home/Home.js";
import Characters from "views/Characters/Characters";
import IndividualCharacter from "views/Characters/IndividualCharacter";
import Episodes from "views/Episodes/Episodes";
import Locations from "views/Locations/Locations";
import Episode from "views/Episodes/Episode";
import IndividualLocations from "./views/Locations/IndividualLocations";

const Routes = [
  {
    path: "/home",
    name: "Home",
    icon: Dashboard,
    component: Home,
    layout: ""
  },
  {
    path: "/characters",
    name: "Characters",
    icon: Person,
    component: Characters,
    layout: ""
  },
  {
    path: "/character/:id",
    name: "Character",
    icon: Person,
    invisible: true,
    component: IndividualCharacter,
    layout: ""
  },
  {
    path: "/locations",
    name: "Locations",
    icon: ExploreIcon,
    component: Locations,
    layout: ""
  },
  {
    path: "/location/:id",
    name: "Location",
    icon: ExploreIcon,
    invisible: true,
    component: IndividualLocations,
    layout: ""
  },
  {
    path: "/episodes",
    name: "Episodes",
    icon: TheatersIcon,
    component: Episodes,
    layout: ""
  },
  {
    path: "/episode/:id",
    name: "Episode",
    icon: TheatersIcon,
    invisible: true,
    component: Episode,
    layout: ""
  },
];

export default Routes;
