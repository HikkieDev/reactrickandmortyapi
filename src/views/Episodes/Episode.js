import React from "react";
import {gql, useQuery} from "@apollo/client";
import {Redirect} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import styles from "./css/Episode"
import {Grid} from "@material-ui/core";
import CharacterCard from "../Characters/inc/CharacterCard";
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles(styles);

export default function Episode(props) {
    const classes = useStyles();

    let {loading, error, data} = useQuery(gql`
                query ($id: ID!)
                {
                    episode(id: $id) {
                        name,
                        episode,
                        air_date,
                        characters {
                            id,
                            name,
                            status,
                            location {
                                name
                            },
                            gender,
                            species,
                            image,
                        }
                    }
                }
        `, {
            variables: {id: props.match.params.id}
        }
    );

    if (loading) return <p>Loading...</p>;
    if (error) return <Redirect from="episode/:id" to="/episodes"/>; //episode doesn't exist

    return (
        <>
            <h1 className={classes.noMargin}>{data.episode.name} - {data.episode.episode}</h1>
            <small>{data.episode.air_date}</small>
            <Divider/>
            <h1 className={classes.noMarginCenter}>Character Appearances</h1>
            <Divider/>
            <Grid container justify="center">
                {
                    data.episode.characters.map(({ id, name, status, location, image, species, gender }) => (
                        <CharacterCard
                            id={id}
                            name={name}
                            status={status}
                            location={location}
                            image={image}
                            species={species}
                            gender={gender}
                        />
                    ))
                }
            </Grid>
        </>
    )
}