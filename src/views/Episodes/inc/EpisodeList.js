import React from "react";
import {gql, useQuery} from "@apollo/client";
import EpisodeCard from "./EpisodeCard";

export default function EpisodeList(props) {

    let { loading, error, data } = useQuery(gql`
        query ($page: Int, $filter: String)
        {
            episodes(page: $page, filter: {name: $filter}) {
                info {
                    pages
                }
                results {
                    id,
                    name,
                    episode,
                    air_date,
                }
            }
        }
    `, {
        variables: { page: props.page, filter: props.filter }
        }
    );

    if (loading) return <p>Loading...</p>;
    if (error) {
        props.MaxPageCallback(1);
        return <p>No Results</p>;
    }

    props.MaxPageCallback(data.episodes.info.pages)

    return data.episodes.results.map(({ id, name, episode, air_date }) => (
        <EpisodeCard
            id={id}
            name={name}
            episode={episode}
            air_date={air_date}
        />
    ))
}