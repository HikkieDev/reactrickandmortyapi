import React from "react";
import {gql, useQuery} from "@apollo/client";
import Loadingimg from "../../assets/img/loadingportal.gif";
import FlexRow from "../../components/Flex/FlexRow";
import Success from "../../components/Typography/Success";
import Danger from "../../components/Typography/Danger";
import Warning from "../../components/Typography/Warning";
import {Divider, Grid} from "@material-ui/core";
import EpisodeCard from "../Episodes/inc/EpisodeCard";
import Card from "../../components/Card/Card";
import CardBody from "../../components/Card/CardBody";

export default function Character(props) {
    let {loading, error, data} = useQuery(gql`
                query ($id: ID!)
                {
                    character(id: $id) {
                        id,
                        name,
                        status,
                        species,
                        type,
                        gender,
                        image,
                        origin {
                            name,
                        },
                        location {
                            id,
                            name,
                        },
                        episode {
                            id,
                            name,
                            episode,
                            air_date,
                        },
                    }
                }
        `, {
            variables: {id: props.match.params.id}
        }
    );

    if (loading) return <FlexRow><img src={Loadingimg} alt="Loading..."/></FlexRow>;
    if (error) return <p>No Results...</p>;

    return (
        <>
            <Card>
                <CardBody>
                    <h1>{data.character.name}</h1>
                    {
                        data.character.status === "Alive" ?
                            (
                                <Success>{data.character.status}</Success>
                            ) : data.character.status === "Dead" ?
                            (
                                <Danger>{data.character.status}</Danger>
                            ) :
                            (
                                <Warning>{data.character.status}</Warning>
                            )
                    }
                    <img alt="Rickimg" src={data.character.image}/>
                    <h5>Species: {data.character.species}</h5>
                    <h5>Type: {data.character.type}</h5>
                    <h5>Gender: {data.character.gender}</h5>
                    <h5>Origin: {data.character.origin.name}</h5>
                    <h5>Last known location: {data.character.location.name}</h5>
                </CardBody>
            </Card>
                <Divider/>
                <h1 className="text-center">Appears in: </h1>
                <Divider/>
            <Grid container justify="center">
                {
                    data.character.episode.map(({id, name, episode, air_date}) => (
                        <EpisodeCard
                            id={id}
                            name={name}
                            episode={episode}
                            air_date={air_date}
                        />
                    ))
                }
            </Grid>

        </>
    )
}