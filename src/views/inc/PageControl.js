import React from "react";
import {Grid} from "@material-ui/core";


export default class PageControl extends React.Component {

    constructor(props) {
        super(props);
        this.next = this.next.bind(this);
        this.prev = this.prev.bind(this);
    }

    next() {
        this.props.container.setState(state => ({
            page: state.page + 1
        }))
    }

    prev() {
        this.props.container.setState(state => ({
            page: state.page - 1
        }))
    }

    go_to_page(page) {
        this.props.container.setState(state => ({
            page: state.page = page
        }))
    }

    render() {
        if (!this.props.max === 1) return null;

        const pages = [];

        for (var i = 1; i <= this.props.max; i++) {
            pages.push(i);
        }

        return (
            <Grid>
                <nav aria-label="Countries Pagination">
                    <ul className="pagination justify-content-center">
                        {this.props.page !== this.props.min &&
                            <li key={this.props.min} className="page-item">
                                <button className="page-link" aria-label="Previous" onClick={this.prev}>
                                    <span aria-hidden="true">&laquo;</span>
                                    <span className="sr-only">Previous</span>
                                </button>
                            </li>
                        }

                        {pages.map((page, index) => {

                            //Check if more than 7
                            if(this.props.max > 7){

                                //Always show first and last page
                                if(page === 1 || page === this.props.max){
                                    return (
                                        <li key={index}
                                            className={`page-item${this.props.container.state.page === page ? ' active' : ''}`}>
                                            <button className="page-link" onClick={() => this.go_to_page(page)}>{page}</button>
                                        </li>
                                    );
                                }else{

                                    //Check if the page is on the first 5 or last 3
                                    if(this.props.page >= 5 && this.props.page < this.props.max - 3){

                                        //Check if the page is less then or more then 2
                                        if(this.props.page - 2 === page || this.props.page + 2 === page){
                                            return (
                                                <li
                                                    className={`page-item${this.props.container.state.page === page ? ' active' : ''}`}>
                                                    <button className="page-link">.</button>
                                                </li>
                                            )
                                        }else{

                                            //Check if the page is less then or more then 1
                                            if(this.props.page - 1 <= page && this.props.page + 1 >= page){
                                                return (
                                                    <li key={index}
                                                        className={`page-item${this.props.container.state.page === page ? ' active' : ''}`}>
                                                        <button className="page-link" onClick={() => this.go_to_page(page)}>{page}</button>
                                                    </li>
                                                );
                                            }else{
                                                return null;
                                            }
                                        }
                                    }else{

                                        //Check if the current page is less then 5
                                        if(this.props.page < 5){

                                            //Check if the index is lower then 5
                                            if(index < 5){
                                                return (
                                                    <li key={index}
                                                        className={`page-item${this.props.container.state.page === page ? ' active' : ''}`}>
                                                        <button className="page-link" onClick={() => this.go_to_page(page)}>{page}</button>
                                                    </li>
                                                );
                                            }else{

                                                //Check if the index is lower then 6
                                                if(index < 6) {
                                                    return (
                                                        <li
                                                            className={`page-item${this.props.container.state.page === page ? ' active' : ''}`}>
                                                            <button className="page-link">.</button>
                                                        </li>
                                                    )
                                                }else{
                                                    return null;
                                                }
                                            }

                                            //Check if the max pages ( -5 ) is greater then page
                                        }else if(page > this.props.max - 5) {
                                            return (
                                                <li key={index}
                                                    className={`page-item${this.props.container.state.page === page ? ' active' : ''}`}>
                                                    <button className="page-link" onClick={() => this.go_to_page(page)}>{page}</button>
                                                </li>
                                            );
                                        }else {

                                            //Check if index is lower then max pages ( -7 )
                                            if(index > this.props.max - 7) {
                                                return (
                                                    <li
                                                        className={`page-item${this.props.container.state.page === page ? ' active' : ''}`}>
                                                        <button className="page-link">.</button>
                                                    </li>
                                                )
                                            }else{
                                                return null;
                                            }
                                        }
                                    }
                                }

                            }else{
                                return (
                                    <li key={index}
                                        className={`page-item${this.props.container.state.page === page ? ' active' : ''}`}>
                                        <button className="page-link" onClick={() => this.go_to_page(page)}>{page}</button>
                                    </li>
                                );
                            }
                        })}

                        
                        {this.props.page !== this.props.max &&
                        <li key={this.props.max} className="page-item">
                            <button className="page-link" aria-label="Next" onClick={this.next}>
                                <span aria-hidden="true">&raquo;</span>
                                <span className="sr-only">Next</span>
                            </button>
                        </li>
                        }
                    </ul>
                </nav>
            </Grid>
        )
    }
}