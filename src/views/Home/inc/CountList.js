import React from "react";
import {gql, useQuery} from "@apollo/client";
import Card from "../../../components/Card/Card";
import GridItem from "../../../components/Grid/GridItem";
import CardHeader from "../../../components/Card/CardHeader";
import CardIcon from "../../../components/Card/CardIcon";
import { makeStyles } from "@material-ui/core/styles";
import GridContainer from "../../../components/Grid/GridContainer";
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";

import Person from "@material-ui/icons/Person";
import ExploreIcon from '@material-ui/icons/Explore';
import TheatersIcon from '@material-ui/icons/Theaters';

export default function CountList(props) {

    let useStyles = makeStyles(styles);
    let classes = useStyles();
    let { loading, error, data } = useQuery(gql`
        query
        {
            episodes {
                info {
                    count
                }
            },
            characters {
                info {
                    count
                }
            },
            locations {
                info {
                    count
                }
            }
        }
    `
    );
    if (loading) return <div>
        <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
                <Card>
                    <CardHeader color="warning" stats icon>
                        <h3>Loading...</h3>
                    </CardHeader>
                </Card>
            </GridItem>
        </GridContainer>
    </div>;

    if (error) return <div>
        <GridContainer>
            <GridItem xs={12} sm={12} md={12}>
                <Card>
                    <CardHeader color="danger" stats icon>
                        <h3>No results...</h3>
                    </CardHeader>
                </Card>
            </GridItem>
        </GridContainer>
    </div>;

    props.CountCallBack(data.characters.info.count)
    return (
        <div>
            <GridContainer>
                <GridItem xs={12} sm={4} md={4}>
                    <Card>
                        <CardHeader color="warning" stats icon>
                            <CardIcon color="warning">
                                <TheatersIcon/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Episodes</p>
                            <h3 className={classes.cardTitle}>
                                {data.episodes.info.count}
                            </h3>
                        </CardHeader>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={4} md={4}>
                    <Card>
                        <CardHeader color="success" stats icon>
                            <CardIcon color="success">
                                <Person/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Characters</p>
                            <h3 className={classes.cardTitle}>
                                {data.characters.info.count}
                            </h3>
                        </CardHeader>
                    </Card>
                </GridItem>
                <GridItem xs={12} sm={4} md={4}>
                    <Card>
                        <CardHeader color="danger" stats icon>
                            <CardIcon color="danger">
                                <ExploreIcon/>
                            </CardIcon>
                            <p className={classes.cardCategory}>Locations</p>
                            <h3 className={classes.cardTitle}>
                                {data.locations.info.count}
                            </h3>
                        </CardHeader>
                    </Card>
                </GridItem>
            </GridContainer>
        </div>
    )
}