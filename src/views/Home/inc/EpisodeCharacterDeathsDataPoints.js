import React from "react";
import ChartistGraph from "react-chartist";
import {gql, useQuery} from "@apollo/client";
import CardHeader from "../../../components/Card/CardHeader";

import {
    CharacterDeathDataPoint
} from "variables/charts.js";

export default function EpisodeCharacterDeathsDataPoint(props) {

    let {loading, error, data} = useQuery(gql`
                query ($page: Int)
                {
                    episodes(page: $page) {
                        info {
                            pages
                        }
                        results {
                            id,
                            episode
                            characters {
                                status
                            }
                        }
                    }
                }
        `,{
            variables: { page: props.page }
        }
    );

    if (loading) return <CardHeader color="warning"><h2><b>Loading....</b></h2></CardHeader>;
    if (error) return <CardHeader color="danger"><h2><b>No Results...</b></h2></CardHeader>;

    props.MaxPageCallback(data.episodes.info.pages)

    CharacterDeathDataPoint.data.labels = [];
    CharacterDeathDataPoint.data.series[0] = [];

    for (let i = 0; i < data.episodes.results.length; i++) {
        var deaths = 0;
        for (let char = 0; char < data.episodes.results[i].characters.length; char++) {
            if(data.episodes.results[i].characters[char].status === "Dead"){
                deaths++;
            }
        }

        CharacterDeathDataPoint.data.labels.push(data.episodes.results[i].episode);
        CharacterDeathDataPoint.data.series[0].push(deaths);
        if (CharacterDeathDataPoint.options.high < deaths) {
            CharacterDeathDataPoint.options.high = data.episodes.results[i].characters.length
        }
    }

    return (
        <CardHeader color="danger">
            <ChartistGraph
                className="ct-chart"
                data={CharacterDeathDataPoint.data}
                type="Line"
                options={CharacterDeathDataPoint.options}
                listener={CharacterDeathDataPoint.animation}
            />
        </CardHeader>
    )
}