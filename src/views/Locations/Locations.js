import React from "react";
import "assets/css/cardbox.css";
import {Grid} from "@material-ui/core";
import LocationsList from "./inc/LocationsList";
import PageControl from "../inc/PageControl";
import {FilterContext} from "../../components/FilterContext";

export default class Locations extends React.Component {

    static contextType = FilterContext;

    constructor(props) {
        super(props);
        this.state = {page: 1, MaxPage: 0};
        this.handleNewMaxPage = this.handleNewMaxPage.bind(this);
        this.setPage = this.setPage.bind(this);
    }

    setPage(value) {
        this.setState( state => ({
            page:value,
        }))
    }

    handleNewMaxPage(value) {
        if (this.state.maxPage !== value) {
            this.setState( state => ({
                maxPage: value,
                page: 1
            }))
        }
    }

    render() {
        const { filter } = this.context

        return (
            <>
                <PageControl container={this} min={1} max={this.state.maxPage} page={this.state.page}/>
                <Grid container justify="center">
                    <LocationsList page={this.state.page} setPage={this.setPage} MaxPageCallback={this.handleNewMaxPage} filter={filter}/>
                </Grid>
            </>
        )
    }
}
