import React from "react";
import Card from "../../../components/Card/Card";
import CardBody from "../../../components/Card/CardBody";
import Button from "../../../components/CustomButtons/Button";
import {makeStyles} from "@material-ui/core/styles";
import styles from "../../../assets/jss/material-dashboard-react/cardImagesStyles";
import {Link} from "react-router-dom";

const useStyles = makeStyles(styles);

export default function LocationCard(props){
    const classes = useStyles();
    return(
        <Card className={classes.card}>
            <CardBody>
                <h4>{props.name}</h4>
                <p>{props.dimension} - {props.type}</p>
                <Button color="success" component={Link} to={"/location/"+props.id}>More Info</Button>
            </CardBody>
        </Card>
    )
}