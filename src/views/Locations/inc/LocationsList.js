import React from "react";
import {gql, useQuery} from "@apollo/client";
import Loadingimg from "../../../assets/img/loadingportal.gif";
import LocationCard from "./LocationCard";

export default function CardBasic(props) {


    let {loading, error, data} = useQuery(gql`
        query ($page: Int, $filter: String)
        {
            locations(page: $page, filter: {name:$filter}){
                info {
                    pages
                },
                results {
                    id,
                    name,
                    type,
                    dimension
                }
            }
        }
    `, {
            variables: {page: props.page, filter: props.filter}
        }
    );
    if (loading) return <img src={Loadingimg}  alt="Loading..." />;
    if (error) {
        props.MaxPageCallback(1);
        return <p>No Results</p>;
    }

    props.MaxPageCallback(data.locations.info.pages)

    return data.locations.results.map(({id, name, type, dimension}) => (
       <LocationCard
           name={name}
           type={type}
           dimension={dimension}
           id={id}
       />
        ));
}